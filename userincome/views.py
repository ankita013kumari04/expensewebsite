from django.shortcuts import render , redirect
from .models import Source, UserIncome
from django.core.paginator import Paginator
from userpreferences.models import UserPreference
from django.contrib import messages
from django.contrib.auth.decorators import login_required
import json
from django.http import JsonResponse

# Create your views here.

@login_required(login_url= '/authentication/login')
def index(request):
    sources = Source.objects.all()
    income = UserIncome.objects.filter(owner = request.user)
    paginator = Paginator(income, 3)
    page_number = request.GET.get('page')
    page_obj = Paginator.get_page(paginator, page_number)
    currency = UserPreference.objects.get(user=request.user).currency
    #currency = UserPreference.objects.filter(user = request.user).exists()
    
    context ={
        'income' :income,
        'page_obj' : page_obj,
        'currency' : currency,
    }
    return render(request,'income/index.html',context)

@login_required(login_url= '/authentication/login')
def add_income(request):
    sources = Source.objects.all()
    context ={
        'sources': sources,
        'values': request.POST
    }
    if request.method =='GET':
        return render(request,'income/add_income.html',context)

    if request.method == 'POST':
        amount = request.POST['amount']
        
        if not amount:
            messages.error(request, 'Amount is required')
            return render(request,'income/add_income.html',context)
        description = request.POST['description']  
        date = request.POST['income_date']  
        source = request.POST['source']  

        if not description:
            messages.error(request, 'description is required')
            return render(request,'income/add_income.html',context)
        if not date:
            messages.error(request, 'date is required')
            return render(request,'income/add_income.html',context)

        UserIncome.objects.create(owner= request.user ,amount=amount,date =date, description= description,source=source)
        messages.success(request, 'income is saved')
        return redirect('income')


def income_edit(request , id):
    income = UserIncome.objects.get(pk =id)
    source = Source.objects.all()
    context ={
        'income':income,
        'values':income,
        'sources':source,
    }

    context_change ={
        'income':income,
        'values':request.POST,
        'sources':source,
    }
    if request.method == 'GET':
        return render(request,'income/edit_income.html', context)

    if request.method == 'POST':
        amount = request.POST['amount']
        description = request.POST['description']  
        date = request.POST['income_date'] 
        source = request.POST['source']  
        
        if not amount:
            messages.error(request, 'Amount is required')
            return render(request,'income/edit_income.html',context_change)

        if not description:
            messages.error(request, 'description is required')
            return render(request,'income/edit_income.html',context_change)
        if not date:
            messages.error(request, 'date is required')
            return render(request,'income/edit_income.html',context_change)

        income.owner =request.user
        income.amount =amount
        income.date =date
        income.description =description
        income.source =source

        income.save()

        messages.success(request, 'income is updated successfully')
        return redirect('income')

def income_delete(request,id):
    income = UserIncome.objects.get(pk=id)
    income.delete()
    messages.success(request, 'income got deleted')
    return redirect('income')

def search_income(request):
    if request.method== 'POST':
        search_str = json.loads(request.body).get('searchText')
        income = UserIncome.objects.filter(
            amount__istartswith = search_str , owner=request.user)|UserIncome.objects.filter(
            date__istartswith = search_str , owner=request.user)|UserIncome.objects.filter(
            description__icontains = search_str , owner=request.user)|UserIncome.objects.filter(
            source__icontains = search_str , owner=request.user)

        data = income.values()
        return JsonResponse(list(data), safe=False)
