from django.shortcuts import render
from django.contrib.auth.models import User
import json
from django.http import JsonResponse
from django.contrib.auth.models import User
from validate_email import validate_email
from django.contrib import messages
from django.core.mail import EmailMessage
from django.contrib import auth
from django.shortcuts import redirect
import threading
from django.views import View
# Create your views here.




class LoginView(View):
    def get(self, request):
        return render(request,'authentication/login.html')

    def post(self,request):
        username = request.POST['username']
        password = request.POST['password']
        
        if username and password:
            user = auth.authenticate(username = username , password = password)
            if user:
                if user.is_active:
                    auth.login(request ,user)
                    messages.success(request,'Welcome,'+user.username+'You are now logged in')
                    return redirect('expenses')
           
            messages.error(request, 'invalid credintial please try again')
            return render(request,'authentication/login.html')

        messages.error(request, 'Please fill all fields')
        return render(request,'authentication/login.html')



class LogoutView(View):
    def post(self , request):
        auth.logout(request)
        messages.success(request, 'You have been logged out')
        return redirect('login')


class EmailValidationView(View):
    def post(self, request):
        data = json.loads(request.body)
        email = data['email']
        if not validate_email(email):
            return JsonResponse({'email_error': 'Invalid email ID'}, status=400)
        if User.objects.filter(email=email).exists():
            return JsonResponse({'email_error': 'sorry email in use,choose another one '}, status=409)
        return JsonResponse({'email_valid': True})
 

class UsernameValidationView(View):
    def post(self, request):
        data = json.loads(request.body)
        username = data['username']
        if not str(username).isalnum():
            return JsonResponse({'username_error': 'username should only contain alphanumeric characters'}, status=400)
        if User.objects.filter(username=username).exists():
            return JsonResponse({'username_error': 'sorry username in use,choose another one '}, status=409)
        return JsonResponse({'username_valid': True})
 
class RegistrationView(View):
    def get(self,request):
        return render(request,'authentication/register.html')

    def post(self,request):
        #GET USER DATA
        #VALIDATE
        #CREATE USER ACCOUNT

        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']   
        context ={
            'fieldValues' : request.POST
        }

        if not User.objects.filter(username = 'username').exists():
            if not User.objects.filter(email = 'email').exists():
                if len(password)<6 :
                    messages.error(request,'password is too short')
                    return render(request,'authentication/register.html',context )

                user = User.objects.create_user(username= username, email = email)
                user.set_password(password)
                user.is_active = True
                user.save()
                messages.success(request , "account created successfully")
                return render(request, 'authentication/register.html')

class RequestPasswordResetEmail(View):
    def get(self , request):
        return render(request,'authentication/reset-password.html')
    def post(self , request):
        email = request.POST['email']
        if not validate_email(email):
            messages.error(request,'Please enter correct email')
        return render(request,'authentication/reset-password.html')


class EmailThread(threading.Thread):
    def __init__(self, email):
        self.email = email
        threading.Thread.__init__(self)

    def run(self):
        self.email.send(fail_silently = False)
